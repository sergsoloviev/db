package db

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
	"github.com/mediocregopher/radix.v2/pool"
)

var (
	DB    *sql.DB
	Redis *pool.Pool
)

type DbInfo struct {
	Type     string `json:"type"`
	Host     string `json:"host"`
	User     string `json:"user"`
	Password string `json:"password"`
	Dbname   string `json:"dbname"`
	Sslmode  string `json:"sslmode"`
	Port     int    `json:"port"`
}

type RedisInfo struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	Pool     int    `json:"pool"`
	Password string `json:"password"`
	Ttl      int    `json:"ttl"`
}

func Connect(o DbInfo) {
	info := fmt.Sprintf(
		"host=%s dbname=%s sslmode=%s user=%s password=%s",
		o.Host, o.Dbname, o.Sslmode, o.User, o.Password,
	)
	var err error
	DB, err = sql.Open(o.Type, info)
	if err != nil {
		panic(err)
		return
	}
	err = DB.Ping()
	if err != nil {
		panic(err)
	}
}

func Disconnect() {
	DB.Close()
}

func ConnectRedis(o RedisInfo) {
	info := fmt.Sprintf("%s:%d", o.Host, o.Port)
	var err error
	Redis, err = pool.New("tcp", info, o.Pool)
	if err != nil {
		panic(err)
	}
}
